const mongoose = require('mongoose');


const loadsSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    enum: [
      'NEW',
      'POSTED',
      'ASSIGNED',
      'SHIPPED',
    ],
    default: 'NEW',
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    enum: [
      'Ready to Pick Up',
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to Delivery',
      'Arrived to Delivery',
    ],
    default: 'Ready to Pick Up',
  },
  dimensions: {
    width: {type: Number, required: true},
    height: {type: Number, required: true},
    length: {type: Number, required: true},
  },
  logs: [{
    _id: false,
    message: {type: String, required: true},
    time: {type: Date, default: Date.now},
  }],

  created_date: {
    type: Date,
    default: Date.now(),
  },
});


module.exports.Load = mongoose.model('Load', loadsSchema);
