const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const getUserInfo = async (req, res) => {
  const myUser = await User.findById(req.user._id, 'email createdDate');
  console.log(myUser);

  res.status(200).json({
    user: {
      _id: myUser._id,
      email: myUser.email,
      createdDate: myUser.createdDate,
    },
  });
};

const deleteUser = async (req, res) => {
  await User.findByIdAndRemove(req.user._id, () => {
    res.status(200).json({message: 'Success'});
  });
};

const changeUserPassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'wrong password'});
  }
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Success'});
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
