const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers');
const {authMiddleware,
  modelExists,
  modelShipper,
  modelDriver} = require('./middleware/authMiddleware');

const {isLoadExists} = require('./middleware/loadsMiddleware');
// const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');
const {Load} = require('../models/loadsModel')
;


const {
  addLoad,
  getUserLoads,
  getUserLoad,
  deleteUserLoad,
  editUserLoad,
  postLoad,
  getShippingInfo,
  getActiveLoads,
  changeDriverActiveLoadInfo} = require('../controllers/loadsContoller');

router.post('/loads', authMiddleware, asyncWrapper(addLoad));

router.get('/loads', authMiddleware,
    asyncWrapper(getUserLoads));
;

router.get('/loads/active', authMiddleware, modelDriver(User),
    asyncWrapper(getActiveLoads));
;

router.patch(
    '/loads/active/state',
    authMiddleware,
    modelDriver(User),
    asyncWrapper(changeDriverActiveLoadInfo),
);


router.get('/loads/:id', authMiddleware,
    isLoadExists(Load), asyncWrapper(getUserLoad));

router.delete('/loads/:id', authMiddleware,
    modelShipper(User), isLoadExists(Load), asyncWrapper(deleteUserLoad));


router.put('/loads/:id', authMiddleware,
    modelShipper(User), isLoadExists(Load), asyncWrapper(editUserLoad));

router.post('/loads/:id/post', authMiddleware,
    modelExists(Load), asyncWrapper(postLoad));

router.get('/loads/:id/shipping_info', authMiddleware, modelShipper(User),
    isLoadExists(Load), asyncWrapper(getShippingInfo));


module.exports = router;
