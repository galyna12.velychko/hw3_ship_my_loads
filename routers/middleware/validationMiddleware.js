const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email({tlds: {allow: false}}),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{1,80}$')),
    role: Joi.string().valid('SHIPPER', 'DRIVER') .required(),
  });
  await schema.validateAsync(req.body);
  next();
};
