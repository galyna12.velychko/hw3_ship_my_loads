const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers');
const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');


const {authMiddleware,
  modelExists,
  modelDriver} = require('./middleware/authMiddleware');

const {
  addTruck,
  getAllTrucks,
  getUserTruck,
  editUserTruck,
  swichCheckUserTruck,
  deleteUserTruck} = require('../controllers/trucksContoller');

router.post('/trucks', authMiddleware,
    modelDriver(User), asyncWrapper(addTruck));
router.get('/trucks', authMiddleware,
    modelDriver(User), asyncWrapper(getAllTrucks));
router.get('/trucks/:id', authMiddleware,
    modelDriver(User), modelExists(Truck), asyncWrapper(getUserTruck));
router.delete('/trucks/:id', authMiddleware,
    modelDriver(User), modelExists(Truck), asyncWrapper(deleteUserTruck));
router.put('/trucks/:id', authMiddleware,
    modelDriver(User), modelExists(Truck), asyncWrapper(editUserTruck));
router.post('/trucks/:id/assign', authMiddleware,
    modelDriver(User), modelExists(Truck), asyncWrapper(swichCheckUserTruck));

module.exports = router;
