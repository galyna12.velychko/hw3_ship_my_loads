const {Truck} = require('../../models/truckModel');

const findFreeTruck = async () => {
  try {
    const truckList = await Truck.find({status: 'IS'});

    if (truckList.length === 0) return null;

    return truckList;
  } catch (err) {
    throw new Error(err.message);
  }
};

module.exports = findFreeTruck;
