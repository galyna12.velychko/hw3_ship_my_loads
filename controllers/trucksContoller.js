const {Truck} = require('../models/truckModel');
const ResponseError = require('../errors/responseError');
const carsTypes = require('./service/trackParamets');

module.exports.addTruck = async (req, res) => {
  const type = req.body.type.toUpperCase();

  if (
    type === 'SPRINTER' ||
        type === 'SMALL STRAIGHT' ||
        type === 'LARGE STRAIGHT'
  ) {
    const truck = new Truck({
      created_by: req.user._id,
      type,
      dimensions: carsTypes[type].dimensions,
      payload: carsTypes[type].payload,
    });

    await truck.save();
    return res.status(200).json({message: 'Truck created successfully'});
  } else return res.status(400).json({message: `Type not found`});
};

module.exports.getAllTrucks = async (req, res) => {
  const userId = req.user._id;
  const {skip = 0, limit = 5} = req.query;
  const trucks = await Truck.find(
      {created_by: userId},
      {__v: 0},
      {
        skip,
        limit: +limit > 100 ? 5 : +limit,
        sort: {
          created_date: -1,
        },
      },
  );

  res.status(200).json({trucks: trucks});
};

module.exports.getUserTruck = async (req, res) => {
  const {id} = req.params;
  const myTruck = await Truck.findById(id, {__v: 0});
  res.status(200).json({truck: myTruck});
};

module.exports.deleteUserTruck = async (req, res) => {
  const {id} = req.params;
  console.log(id);
  await Truck.findByIdAndRemove({_id: id});
  res.status(200).json({message: 'Truck has been deleted'});
};

module.exports.editUserTruck = async (req, res) => {
  const {id} = req.params;
  const type = req.body.type.toUpperCase();
  if (
    type === 'SPRINTER' ||
        type === 'SMALL STRAIGHT' ||
        type === 'LARGE STRAIGHT'
  ) {
    const myTruck = await Truck.findById(id);
    myTruck.type = type;
    await myTruck.save();
    res.status(200).json({message: 'Truck details changed successfully'});
  } else return res.status(400).json({message: `Type not found`});
};

module.exports.swichCheckUserTruck = async (req, res) => {
  const {id} = req.params;
  const userId = req.user._id;

  const trucks = await Truck.find({created_by: userId}, 'assigned_to');
  const myTruck = await Truck.findById(id);
  trucks.forEach((el) => {
    if (el.assigned_to) {
      throw new ResponseError('One truck is already assigned');
    }
  });

  myTruck.assigned_to = userId;
  await myTruck.save();
  res.status(200).json({message: 'Truck assigned successfully'});
};
